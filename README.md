# Test Project for Easy Pallet (Back-End)

**Easy Pallet Test Project developed by Ricardo Lima**

> O projeto consistiu na criação de uma aplicação Rails API, duas entidades foram criadas, Loads (Cargas) e Orders (Pedidos), onde ambas são alimentadas via uma rake task de nome utils (utils.rake) utilizando rest client, que executa a comunicação com os endpoints e popula as entidades.

> É utilizada a _gem whenever_ para realizar a automatização do tempo da leitura e carga dos dados fornecidos pelos endpoints. No arquivo schedule.rb possui a regra que faz basicamente a cada 5 minutos executar uma sequência de comandos do rails: db:drop, db:create, db:migrate, utils:loads e utils:orders.

![Easy Pallet](./public/logo.png 'Easy Pallet')

## Dependecies

Must have installed:

- Ruby version 2.3.3
- Rails version 5.0.1
- Mysql 5.7

> Note: In mysql it is necessary to use the **root** user with password **123456** in development, or go to database.yml and set the username and password as needed.

## Project setup

Clone the project:

```
git clone git@gitlab.com:ricardo.dias/easy-pallet.git
```

Enter the project folder:

```
cd easy-pallet/
```

Run the bundle:

```
bundle install
```

Run the following command line:

```
rails db:create db:migrate utils:loads utils:orders
```

### Run the server under development

```
rails s
```

### Access URIs endpoints

```
http://localhost:3000/api/v1/loads
```

and

```
http://localhost:3000/api/v1/orders
```

# Import Manual CSV file

Execute in terminal the following command:

```
rails csv:import
```

> Note: Wait a little... ;)

# Enable cron start

If you need to run the command:

```
whenever --update-crontab
```
