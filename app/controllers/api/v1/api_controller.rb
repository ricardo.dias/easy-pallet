require 'rest-client'

module Api::V1
    class ApiController < ApplicationController
        def api_loads
            loads = RestClient::Request.execute(method: :get, url: 'https://uberlandia.easypallet.app/loads', timeout: 10)
            render json: loads, status: 200
        end
    end
end