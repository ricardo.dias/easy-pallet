class Api::V1::LoadsController < ApplicationController
    def index
        @loads = Load.all
        render json: @loads, status: 200
    end

    def show
        @load = Load.find(params[:id])
        render json: @load, status: 200
    end
end
