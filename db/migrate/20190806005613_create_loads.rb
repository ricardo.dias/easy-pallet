class CreateLoads < ActiveRecord::Migration[5.0]
  def change
    create_table :loads do |t|
      t.string :vehicle
      t.string :code
      t.datetime :delivery_date

      t.timestamps
    end
  end
end
