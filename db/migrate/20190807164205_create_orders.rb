class CreateOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :orders do |t|
      t.string :code
      t.string :bay
      t.string :dock
      t.integer :load_id

      t.timestamps
    end
  end
end
