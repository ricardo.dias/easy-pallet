require 'csv'

namespace :csv do
  desc "Read and write spreadsheet data."
  task import: :environment do
    spinner = TTY::Spinner.new("[:spinner] Loading, please wait...")
    spinner.auto_spin

    CSV.foreach('tmp/dados.csv', col_sep: ';').with_index do |linha, indice|
      unless (indice == 0)
        Load.create!(vehicle: linha[0], code: linha[1], delivery_date: linha[2])
        Order.create!(code: linha[3], bay: linha[4], dock: linha[5], load_id: linha[6] )
      end
    end

    spinner.success('CSV loaded with success!')
  end
end
