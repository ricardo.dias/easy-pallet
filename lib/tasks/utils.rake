require 'rest-client'

namespace :utils do
  desc "Read and write endpoint Loads"
  task loads: :environment do
    spinner = TTY::Spinner.new("[:spinner] Read and writer data loads...")
    spinner.auto_spin

    response = RestClient.get 'https://uberlandia.easypallet.app/loads'
    load_data = JSON.parse(response)

    load_data.each do |load|
      vehicle = load['vehicle']
      code = load['code']
      delivery_date = load['delivery_date']

      Load.create!(vehicle: vehicle, code: code, delivery_date: delivery_date)
    end
    spinner.success('Write data loads executed with success!')
  end

  desc "Read and write endpoint Orders"
  task orders: :environment do
    spinner = TTY::Spinner.new("[:spinner] Read and writer data orders...")
    spinner.auto_spin
    response = RestClient.get 'https://uberlandia.easypallet.app/orders'
    order_data = JSON.parse(response)

    order_data.each do |order|
      code = order['code'] 
      bay = order['bay']
      dock = order['dock'] 
      load_id = order['load_id']

      Order.create!(code: code, bay: bay, dock: dock, load_id: load_id)
    end
    spinner.success('Write data orders executed with success!')
  end
end
